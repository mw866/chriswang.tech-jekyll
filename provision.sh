# For Debian; Do not sudo on MacOs
if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then 
    sudo apt-get install -y ruby-full build-essential patch ruby-dev zlib1g-dev liblzma-dev
fi
gem install bundler
bundle update # Rebuild Gemfile.lock according to the current environmebnt
bundle install --path vendor/bundle # To avoid installation in system RubyGems
