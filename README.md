# chriswang.tech ![Build Status](https://gitlab.com/mw866/chriswang.tech/badges/master/build.svg)

The repo consists of the following components:

*  Jekyll source for static website generation
  * `_config.yml`:  Jekyll configuration
  * `_posts`: the posts in markdown.
  * `_drafts`: 
  * `public`: the generated web pages in HTML, which are excluded in version control and built at every push by Gitlab CI 

<del>

* Travis-CI configuration: `.travis.yml`
* AWS CodeDelpoy configuration: `appspec.yml`

</del>

## Instructions

* Set up local Jekyll environment: execute `provision.sh`
* Build and test `public/`: `bundle exec jekyll build  && htmlproofer ./public`
* Run Jekyll local dev server: `bundle exec jekyll serve —watch —host 0.0.0.0`

<del>

* View AWS CodeDeploy log: `tail -f /var/log/aws/codedeploy-agent/codedeploy-agent.log`

</del>

## References

### Gitlab

* [Build Jekyll with Bundler](https://gitlab.com/jekyll-themes/default-bundler)
* [Gitlab Pages Setu](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#add-gitlab-ci)
* [GitLab Pages custom domains and SSL/TLS Certificates](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html


### Jekyll

* [Jekyll Documentation](https://jekyllrb.com/docs/)
* [Tutorial](https://www.taniarascia.com/make-a-static-website-with-jekyll/)
* [Jekyll-Uno Theme](https://github.com/joshgerdes/jekyll-uno)

### Analytics / SEO

* Google Analytics Tutorial: https://analyticsacademy.withgoogle.com/course/2
* Github SEO plugin: https://help.github.com/articles/search-engine-optimization-for-github-pages/
* Manage Bots traffic on Google Analytics: http://www.globaldots.com/filter-bot-traffic-google-analytics/

#### htmlproofer

* [html-proofer](https://github.com/gjtorikian/html-proofer)

<del>

### AWS CodeDeploy

* [Getting Started with AWS CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-codedeploy.html)
* [Add an Application Specification File to a Revision for AWS CodeDeploy](https://docs.aws.amazon.com/codedeploy/latest/userguide/application-revisions-appspec-file.html)
* [AppSpec File Reference](https://docs.aws.amazon.com/codedeploy/latest/userguide/reference-appspec-file.html)
* [Rename an Application](https://docs.aws.amazon.com/codedeploy/latest/userguide/applications-rename.html)
* [New – Notifications for AWS CodeDeploy Events](https://aws.amazon.com/blogs/aws/new-notifications-for-aws-codedeploy-events/)
### Travis CI

* [Build: Jekyll to Travis CI](https://jekyllrb.com/docs/continuous-integration/travis-ci/)

* [~~Deploy : Upload build aritifacts~~](https://docs.travis-ci.com/user/uploading-artifacts/)

* [Deploy: CodeDeploy](https://docs.travis-ci.com/user/deployment/codedeploy/)
  * Example 1: https://www.keithcaulkins.com/Travis-and-CodeDeploy/
  * Example 2: [Comprehensive AWS EC2 Deployment with TravisCI Guide](https://medium.com/@itsdavidthai/comprehensive-aws-ec2-deployment-with-travisci-guide-7cafa9c754fc)
  * Example: [.travis.yml](https://github.com/airavata-courses/TeamApex/blob/dev/.travis.yml)

* [Configuring Build Notifications](https://docs.travis-ci.com/user/notifications/)

### Email

* Email forwarding from custom domain to to personal address using `improvmx.com`:  https://woorkup.com/free-email-forwarding/~~

</del>


# Troubleshooting

### Local: "An error occurred while installing RedCloth (4.2.9), and Bundler cannot continue. Make sure that `gem install RedCloth -v '4.2.9'` succeeds before bundling." OR "An error occurred while installing nokogiri (1.6.7.2), and Bundler
cannot continue. Make sure that `gem install nokogiri -v '1.6.7.2'` succeeds before bundling"

Solution:
Install dependencies.
`sudo apt-get install build-essential patch`
`sudo apt-get install ruby-dev zlib1g-dev liblzma-dev`
http://www.nokogiri.org/tutorials/installing_nokogiri.html#ubuntu___debian



### Local: An error occurred while installing json (1.8.3), and Bundler cannot continue. Make sure that `gem install json -v '1.8.3'` succeeds before bundling.

Solution:
Run `bundle check` to ensure the `gemfile.lock` is up-to-date with `Gemfile`.

OR 
`rm Gemfile.lock` then `bundle install`
https://gistpages.com/posts/gem-ext-builderror-error-failed-to-build-gem-native-extension-for-the-json-gem

### Gitlab CI: Conversion error: Jekyll::Converters::Scss encountered an error while converting 'assets/css/style.scss': Invalid US-ASCII character "\xE2" on line 5

Solutions: 
Change encoding to UTF-8.
https://github.com/jekyll/jekyll/issues/4268#issuecomment-396165096

### Gitlab CI: Filed to extract & 404 HTTP error at the domain name

Solution: 
The folder must be named `public` instead of `_site` etc.
https://docs.gitlab.com/ce/user/project/pages/getting_started_part_four.html#the-public-directory

### Browser: Multiple 404 returned by .css files e.g. `/css/monokai.css` at `https://mw866.gitlab.io/www.chriswang.tech`
Solution:
`mw866.gitlab.io/css/monokai.css` is invalid due to the addtional path prefixes.
* Option 1: add domain name
* Option 2: change baseurl to in include the path at Jekyll `_config.yml`


<del>

### Codedeploy: ERROR [codedeploy-agent(12820)]: InstanceAgent::Plugins::CodeDeployPlugin::CommandPoller: Missing credentials - please check if this instance was started with an IAM instance profile

Solution:

Create and attach an IAM profile to the EC2 instance.

https://docs.aws.amazon.com/codedeploy/latest/userguide/getting-started-create-iam-instance-profile.html#getting-started-create-iam-instance-profile-console

### Travis: CI prepends build number in S3 path when uploading Build Artifacts

Solution:

| Source: Travis-CI      | Destination: S3                   |
| ---------------------- | --------------------------------- |
| `woring_dir\paths(1)}` | `s3:\\buket_name\target_paths(1)` |
| `working_dir\paths(2)` | `s3:\\buket_name\target_paths(2)` |
| …                      | …                                 |

Reference: https://github.com/travis-ci/docs-travis-ci-com/issues/456

</del>


#### Cloudflare Workers Site
[Start from an Existing Site](https://developers.cloudflare.com/workers/sites/start-from-existing/)