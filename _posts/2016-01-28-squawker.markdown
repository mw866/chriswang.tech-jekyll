---
title:  "Squawker: a social messaging web service"
date:   2017-01-28 15:04:23
categories: [tech]
tags: [web, python, cloud]
comments: true
---

**Squawker** is a social messaging web service inspired by my startup system assignment at Cornell Tech.

[>>Link to Squawker<<](http://squawker.chriswang.tech/)

Note: *Squawker hibernate after 30 minutes of inactivity. Please allow 15 seconds for the site to "wake up".*

## Technology Stack
* CSS: Bootstrap

* Web Framework: Django

* IaaS: Heroku 

* Database: Heroku Postgres SQL

/Chris